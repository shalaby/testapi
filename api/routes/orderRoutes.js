'use strict';
module.exports = function(app) {
  const order = require('../controllers/orderController');

  // order Routes
  app.route('/orders')
      .get(order.list_all_orders)
      .post(order.create_a_order);


  app.route('/orders/:orderId')
      .get(order.read_a_order)
      .put(order.update_a_order)
      .delete(order.delete_a_order);
};