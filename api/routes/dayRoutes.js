'use strict';
module.exports = function(app) {
  const day = require('../controllers/dayController');

  // day Routes
  app.route('/days')
      .get(day.list_all_days)
      .post(day.create_a_day);


  app.route('/days/:dayId')
      .get(day.read_a_day)
      .put(day.update_a_day)
      .delete(day.delete_a_day);

  app.route('/pickup')
      .get(day.list_pickup_days);

  app.route('/delivery')
      .get(day.list_delivery_days);
};