'use strict';


const mysql = require('../../config');

exports.list_all_orders = function(req, res) {
  mysql.query('SELECT * FROM orders', (err,rows) => {
    if(err) res.send(err);
    res.json(rows);
  });
};




exports.create_a_order = function(req, res) {
  const order = req.body;
  const date = new Date();
  mysql.query('INSERT INTO orders SET date = ?', [date], (err, result) => {
    if(err) res.send(err);
    let totalAmount = 0;
    let totalItems = 0;
    order.forEach(item => {
      const row = {
        product_id: item.id,
        quantity: item.quantity,
        order_id: result.insertId
      };
      mysql.query('INSERT INTO products_orders SET ?', row, (error, _result) => {
        if(error) res.send(error);
      });
      item.total = item.price * item.quantity;
      totalAmount += item.total;
      totalItems += item.quantity;
    });
    res.json({order, totalItems, totalAmount});
  });
};


exports.read_a_order = function(req, res) {
  const id = req.params.id;
  mysql.query('SELECT * FROM orders Where id = ?', [id], (err,rows) => {
    if(err) res.send(err);
    res.json(rows[0]);
  });
};


exports.update_a_order = function(req, res) {
  const order = req.body;
  mysql.query(
      'UPDATE orders SET name = ?, price = ? Where id = ?',
      [order.name, order.price, order.id],
      (err, result) => {
        if (err) res.send(err);
        res.json({ message: 'Order successfully updated' });
      }
  );
};


exports.delete_a_order = function(req, res) {
  const id = req.params.orderId;
  mysql.query(
      'DELETE FROM orders WHERE id = ?', [id], (err, result) => {
        if (err) throw res.send(err);
        res.json({ message: 'Order successfully deleted' });
      }
  );
};