'use strict';


const mysql = require('../../config');

exports.list_all_products = function(req, res) {
  mysql.query('SELECT * FROM products', (err,rows) => {
    if(err) res.send(err);
    res.json(rows);
  });
};




exports.create_a_product = function(req, res) {
  const product = req.body;
  mysql.query('INSERT INTO products SET ?', product, (err, _res) => {
    if(err) res.send(err);
    res.json(_res);
  });
};


exports.read_a_product = function(req, res) {
  const id = req.params.id;
  mysql.query('SELECT * FROM products Where id = ?', [id], (err,rows) => {
    if(err) res.send(err);
    res.json(rows[0]);
  });
};


exports.update_a_product = function(req, res) {
  const product = req.body;
  mysql.query(
      'UPDATE products SET name = ?, price = ? Where id = ?',
      [product.name, product.price, product.id],
      (err, result) => {
        if (err) res.send(err);
        res.json({ message: 'Product successfully updated' });
      }
  );
};


exports.delete_a_product = function(req, res) {
  const id = req.params.productId;
  mysql.query(
      'DELETE FROM products WHERE id = ?', [id], (err, result) => {
        if (err) throw res.send(err);
        res.json({ message: 'Product successfully deleted' });
      }
  );
};