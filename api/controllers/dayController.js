'use strict';


const mysql = require('../../config');
const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

exports.list_all_days = function(req, res) {
  mysql.query('SELECT * FROM days', (err,rows) => {
    if(err) res.send(err);
    res.json(rows);
  });
};

exports.list_pickup_days = function(req, res) {
  const startDate = new Date();
  mysql.query('SELECT * FROM days', (err,rows) => {
    if(err) res.send(err);
    const result = [];
    for (let i = 0; i < 7; i++) {
      let currentDate = new Date();
      currentDate.setDate(startDate.getDate() + i);
      const today = currentDate.getDay();
      const day = rows.find(row => row.day === days[today]);
      result.push(day);
    }
    res.json(result);
  });
};

exports.list_delivery_days = function(req, res) {
  const fixDays = 1;
  const startDate = new Date(req.query.date);
  startDate.setDate(startDate.getDate() + fixDays);
  mysql.query('SELECT * FROM days', (err,rows) => {
    if(err) res.send(err);
    const result = [];
    for (let i = 0; i < 7; i++) {
      let currentDate = new Date();
      currentDate.setDate(startDate.getDate() + i);
      const today = currentDate.getDay();
      const day = rows.find(row => row.day === days[today]);
      result.push(day);
    }
    res.json(result);
  });
};




exports.create_a_day = function(req, res) {
  const day = req.body;
  mysql.query('INSERT INTO days SET ?', day, (err, _res) => {
    if(err) res.send(err);
    res.json(_res);
  });
};


exports.read_a_day = function(req, res) {
  const id = req.params.id;
  mysql.query('SELECT * FROM days Where id = ?', [id], (err,rows) => {
    if(err) res.send(err);
    res.json(rows[0]);
  });
};


exports.update_a_day = function(req, res) {
  const day = req.body;
  mysql.query(
      'UPDATE days SET name = ?, price = ? Where id = ?',
      [day.name, day.price, day.id],
      (err, result) => {
        if (err) res.send(err);
        res.json({ message: 'Work Day successfully updated' });
      }
  );
};


exports.delete_a_day = function(req, res) {
  const id = req.params.dayId;
  mysql.query(
      'DELETE FROM days WHERE id = ?', [id], (err, result) => {
        if (err) throw res.send(err);
        res.json({ message: 'Work Day successfully deleted' });
      }
  );
};
