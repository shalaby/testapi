const express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    cors = require('cors');

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


const productRoutes = require('./api/routes/productRoutes');
const orderRoutes = require('./api/routes/orderRoutes');
const dayRoutes = require('./api/routes/dayRoutes');
productRoutes(app);
orderRoutes(app);
dayRoutes(app);


app.listen(port);


console.log('Cart RESTful API server started on: ' + port);