const mysql = require('mysql'),
    connection = mysql.createConnection({
      host: 'localhost',
      user: 'username',
      password: 'password',
      database: 'cart'
    });

connection.connect();

module.exports = connection;